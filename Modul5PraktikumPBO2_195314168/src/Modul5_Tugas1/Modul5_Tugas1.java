package Modul5_Tugas1;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Modul5_Tugas1 extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 300;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private JLabel text1;
    private JLabel text2;
    private JLabel hasil;
    private JButton button_hitung;
    private JTextField panjang;
    private JTextField lebar;
    private JTextField hitung_bilangan;

    public static void main(String[] args) {
        Modul5_Tugas1 frame = new Modul5_Tugas1();
        frame.setVisible(true);
    }

    public Modul5_Tugas1() {
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setTitle("Input Data");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        text1 = new JLabel("Panjang (m)");
        text1.setBounds(0, 15, 100, 60);
        text1.setFont(new Font("Tahoma", 1, 14));
        contentPane.add(text1);

        text2 = new JLabel("Lebar (m)");
        text2.setBounds(0, 65, 80, 60);
        text2.setFont(new Font("Tahoma", 1, 14));
        contentPane.add(text2);

        hasil = new JLabel("Luas (m2)");
        hasil.setBounds(0, 121, 80, 60);
        hasil.setFont(new Font("Tahoma", 1, 14));
        contentPane.add(hasil);

        panjang = new JTextField();
        panjang.setBounds(120, 30, 300, 35);
        contentPane.add(panjang);

        lebar = new JTextField();
        lebar.setBounds(120, 80, 300, 35);
        contentPane.add(lebar);

        hitung_bilangan = new JTextField();
        hitung_bilangan.setBounds(120, 135, 300, 35);
        contentPane.add(hitung_bilangan);

        button_hitung = new JButton("Hitung");
        button_hitung.setBounds(117, 185, 100, 40);
        button_hitung.setFont(new Font("Tahoma", 1, 14));
        contentPane.add(button_hitung);

        button_hitung.addActionListener(this);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        int a, b;

        try {
            a = Integer.parseInt(panjang.getText());
            b = Integer.parseInt(lebar.getText());

            hitung_bilangan.setText("" + (a * b));

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Maaf, hanya integer yang diperbolehkan",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

}
